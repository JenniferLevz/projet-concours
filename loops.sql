-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le : jeu. 09 fév. 2023 à 14:32
-- Version du serveur : 10.4.25-MariaDB
-- Version de PHP : 8.1.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `loops`
--

-- --------------------------------------------------------

--
-- Structure de la table `recettes`
--

CREATE TABLE `recettes` (
  `idr` int(11) NOT NULL COMMENT 'cle primaire',
  `nom` varchar(255) DEFAULT NULL COMMENT 'nom de la recette',
  `ingredients` text NOT NULL COMMENT 'les ingredients',
  `duree` time NOT NULL COMMENT 'la duree de la recette',
  `preparation` text NOT NULL COMMENT 'la preparation'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `recettes`
--

INSERT INTO `recettes` (`idr`, `nom`, `ingredients`, `duree`, `preparation`) VALUES
(3, 'Takoyaki', '250 g de pâte à takoyaki (disponible dans les magasins asiatiques) \r\n\r\n8 à 10 morceaux de tako (poulpe) cuit, coupé en petits dés \r\n\r\n2 oignons verts, finement tranchés \r\n\r\n1 tasse de feuilles de bonite séchées \r\n\r\n2 cuillères à soupe de sauce soja \r\n\r\n1 cuillère à soupe de sauce Worcestershire \r\n\r\n2 cuillères à soupe de mayonnaise \r\n\r\npincée de sel ', '02:00:00', 'Préchauffer un appareil à takoyaki sur feu moyen-élevé. \r\n\r\nDans un bol, mélanger la pâte à takoyaki avec suffisamment d\'eau pour qu\'elle soit lisse. \r\n\r\nDans chaque alvéole de l\'appareil à takoyaki, verser une petite cuillère de pâte. \r\n\r\nAjouter une petite quantité de tako, d\'oignons verts et de feuilles de bonite sur chaque boule de pâte. \r\n\r\nVerser un peu plus de pâte sur le dessus pour couvrir les ingrédients. \r\n\r\nLaisser cuire les takoyaki pendant environ 3 minutes, ou jusqu\'à ce qu\'ils soient dorés et bien cuits à l\'intérieur. \r\n\r\nRetourner les takoyaki avec une fourchette ou un bâtonnet en bois pour cuire l\'autre côté. \r\n\r\nRépéter l\'opération jusqu\'à épuisement de la pâte. \r\n\r\nDans un petit bol, mélanger ensemble la sauce soja, la sauce Worcestershire, la mayonnaise et le sel. \r\n\r\nServir les takoyaki chauds avec la sauce préparée. '),
(4, 'Kakunidon – Donburi de porc braisé ', '2 bols de Riz cuit \r\n\r\n300 g Poitrine de porc non tranchée coupée en gros cubes \r\n\r\n1 Blanc de poireau \r\n\r\n1 c. à soupe d\'Huile \r\n\r\n4 c. à soupe de Sauce soja \r\n\r\n4 c. à soupe de Mirin \r\n\r\n4 c. à soupe de Sake \r\n\r\n2 c. à soupe de Sucre en poudre \r\n\r\nLégumes au choix cuits vapeur ou à l\'eau ', '00:40:00', '1. Inciser le blanc de poireau dans la longueur, dérouler et mettre à plat quelques morceaux \r\n\r\n2. Couper en julienne dans la longueur et mettre à tremper 10/15 mn dans un bol d\'eau froide puis égoutter \r\n\r\n3. Faire dorer chaque cube de viande sur toutes les faces dans l\'huile puis retirer le gras fondu \r\n\r\n4. Ajouter le gingembre, les sauces et le sucre \r\n\r\nPorter à ébullition puis baisser le feu \r\n\r\nCouvrir et laisser cuire sur feu doux pendant 30 mn en retournant régulièrement la viande \r\n\r\n6. La sauce doit avoir bien réduit \r\n\r\n7. Servir sur un bol de riz avec des légumes vapeur sur lesquels on versera de la sauce \r\n\r\n8. Décorer de julienne de poireau \r\n\r\nItadakimasu ! '),
(5, 'NARUTOMAKI', '1 - 600g d’Itoyori ou autre poisson à chair blanche \r\n\r\n2 - 20ml de mirin (saké doux à incorporer dans le bouillon) \r\n\r\n3 - 2 blancs d’œufs \r\n\r\n4 - 10g de sucre \r\n\r\n5 - 10g de sel \r\n\r\n6 - 20g de farine \r\n\r\nEt … 1 colorant alimentaire (rose de préférence) et des glaçons ', '03:00:00', '1. Coupez en petits morceaux la chair du poisson puis dispersez-les dans de l’eau avec des glaçons dans un grand saladier. Remuez et attendez que les morceaux de votre poisson se retrouvent au fond du bol. \r\n\r\n2. Il faut maintenant essorer le poisson. Grâce à un torchon, on va pouvoir essorer très fort le poisson, au creux du tissu. \r\n\r\n3. Utilisez un hachoir afin de couper encore plus finement votre chair de poisson. Ajoutez-y le sel après. \r\n\r\n4. Mixez dans un premier temps votre chair hachée, puis utilisez un pilon ou mortier pour obtenir la pâte de poisson. Elle doit avoir un aspect élastique, caoutchouteux. \r\n\r\n5. Ajoutez le sel, les blancs d’œufs, la farine et le saké doux (ou mirin). Tous ces ingrédients doivent être incorporés à votre mélange. \r\n\r\n6. Pilonnez de nouveau pour homogénéiser la pâte. \r\n\r\n7. Séparez votre pâte en deux (par la tranche), ajoutez le colorant et laissez reposer 1h. \r\n\r\n8. Disposez la pâte dans des ramequins, dans un boudin en film plastique. Cuire pendant 25-30 min à la vapeur. \r\n\r\n9. Stoppez la cuisson en plongeant votre narutomaki dans un bain glacé'),
(6, 'LE MAQUE-BURGER DU YUKIHIRA', '1 bol de Riz blanc déjà cuit \r\n\r\n1 conserve de Maquereaux au naturel ou grillés \r\n\r\n1 Oeuf \r\n\r\n2 c. à soupe de Chapelure \r\n\r\n1 c. à soupe d’ Oignon émincé \r\n\r\n1 c. à soupe de Fécule de pomme de terre \r\n\r\n2 c. à soupe de Sauce ponzu \r\n\r\n1 c. à café d’ Huile ', '01:00:00', '1. Ne pas jeter l’eau des maquereaux ! \r\n\r\n2. Dans un saladier, hacher le maquereau à la fourchette et réserver l’eau dans une casserole \r\n\r\n3. Mélanger l’oeuf, l’oignon et la chapelure \r\n\r\n4. Incorporer au maquereau, saler, poivrer et mélanger \r\n\r\n5. Faire cuire comme un steak des 2 côtés pendant 5 à 10 minutes sur feu moyen \r\n\r\n6. Retourner à l’aide d’une spatule (attention à ne pas le casser !) \r\n\r\n7. Mélanger le jus du maquereau, le ponzu et la fécule de pomme de terre délayée dans 1 cuillère à soupe d’eau \r\n\r\n8. Faire épaissir sur feu doux brièvement \r\n\r\n9. Servir le maque-burger en arrosant de sauce avec le bol de riz \r\n\r\nItadakimasu ! '),
(7, 'KAKI NO TANE-AGE DU YUKIHIRA', '4 pavés de Poisson blanc frais ou surgelés \r\n\r\n1 sachet d\' Apéritif japonais \r\n\r\n2 c. à soupe de Mayonnaise toute prête ou maison \r\n\r\n1 à 2 c. à soupe de Farine \r\n\r\n1 Oeuf \r\n\r\n1 c. à soupe de Poivre Sanshô \r\n\r\nSel et poivre ', '00:50:00', 'Après décongélation (si nécessaire), mettre le poisson entre 2 feuilles d\'essuie-tout pour éliminer le surplus d\'eau \r\n\r\nÉcraser les apéritifs avec un rouleau à patisserie \r\n\r\nIl est aussi possible de les mixer brièvement \r\n\r\nSaler et poivrer le poisson \r\n\r\nDisposer la farine, l\'oeuf battu et les apéritifs mixés dans 3 assiettes creuses \r\n\r\nPasser chaque pavé dans la farine \r\n\r\nPuis l\'oeuf \r\n\r\nPuis la chapelure d\'apéritifs \r\n\r\nMélanger le poivre sanshô avec la mayonnaise \r\n\r\nFaire chauffer de l\'huile de friture à 175°C \r\n\r\nPlonger les pavés 1 par 1 (maxi 2 par 2) et faire cuire jusqu\'à ce qu\'ils soient dorés en les retournant régulièrement \r\n\r\nUne fois cuits, faire absorber le surplus de gras sur de l\'essuie-tout '),
(8, 'ONIGIRI DE MAQUEREAU FAÇON CHAZUKE', '2 petits bols de Riz cuit \r\n\r\n2 tranches de Maquereau frais \r\n\r\nThé vert \r\n\r\n4 c. à soupe d’Huile d’olive \r\n\r\n2 c à soupe de Feuilles de persil ou coriandre frais \r\n\r\nAlgues nori en lamelles \r\n\r\nSel, poivre \r\n ', '01:00:00', '1. Poêler la tranche de maquereau côté peau dans l’huile d’olive \r\n\r\n2. L’arroser régulièrement avec l’huile d’olive \r\n\r\n3. Continuer jusqu’à ce que la peau soit croustillante et l’intérieur cuit \r\n\r\n4. Saler, poivrer, laisser un peu refroidir et couper en dés \r\n\r\n5. Préparer du thé vert, le saler et réserver \r\n\r\n6. Former des onigiri en y insérant des dés de maquereau \r\n\r\n7. Les mettre dans 2 bols et verser le thé dessus \r\n\r\n8. Parsemer de feuilles de persil ou de coriandre et de lamelles d’algues nori \r\n\r\nItadakimasu   ');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `recettes`
--
ALTER TABLE `recettes`
  ADD PRIMARY KEY (`idr`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `recettes`
--
ALTER TABLE `recettes`
  MODIFY `idr` int(11) NOT NULL AUTO_INCREMENT COMMENT 'cle primaire', AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
