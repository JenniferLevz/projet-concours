<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="projet.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js" integrity="sha384-w76AqPfDkMBDXo30jS1Sgez6pr3x5MlQ1ZAGC+nuZB+EYdgRZgiwxhTBTkF7CXvN" crossorigin="anonymous"></script>
   <title>| Reshipi</title>
   <link rel="preconnect" href="https://fonts.googleapis.com">
   <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
   <link href="https://fonts.googleapis.com/css2?family=Julee&display=swap" rel="stylesheet">
   
</head>

<header>
    <nav class="navbar">
      <div class="container-fluid">
        <a class="navbar-brand" href="#">
          <img src="logo_reshipi.png" class="logo" alt="LOGO">
        </a>
        <figure>
          <blockquote class="blockquote">
            <p>RESHIPI </p>
          </blockquote>
          <figcaption class="blockquote-footer">
           Le meilleur de tes recettes d' <cite title="Source Title">Anime </cite> préférés !
        </figcaption></figure>
        <a class="navbar-brand" href="projet.php">Accueil</a>
        <form class="d-flex" role="search">
          <input class="form-control me-2" type="search" placeholder="Rechercher" aria-label="Search">
          <button class="btn btn-outline-success" type="submit">
            <i class="bi bi-search-heart">
              <svg xmlns="http://www.w3.org/2000/svg" class="bi bi-search-heart" fill="currentColor" viewBox="0 0 16 16">
            <path d="M6.5 4.482c1.664-1.673 5.825 1.254 0 5.018-5.825-3.764-1.664-6.69 0-5.018Z"/>
            <path d="M13 6.5a6.471 6.471 0 0 1-1.258 3.844c.04.03.078.062.115.098l3.85 3.85a1 1 0 0 1-1.414 1.415l-3.85-3.85a1.007 1.007 0 0 1-.1-.115h.002A6.5 6.5 0 1 1 13 6.5ZM6.5 12a5.5 5.5 0 1 0 0-11 5.5 5.5 0 0 0 0 11Z"/>
          </svg></i></button>
        </form>
      </div>
    </nav>
  </header>

  <body>
    <form action="" method="post" class="mb">
      <div class="mb-3">
        <label for="Nom" class="form-label">Nom :</label>
        <input type="text" class="form-control" id="Nom" aria-describedby="emailHelp">
      </div>
      <div class="mb-3">
        <label for="Prenom" class="form-label">Prénom :</label>
        <input type="text" class="form-control" id="Prenom" name="Prenom">
        <label for="Mail" class="form-label">Adresse mail :</label>
        <input type="email" class="form-control" id="Mail" aria-describedby="emailHelp"> 
      </div>
      <div class="mb-3">
        <label for="tel" class="form-label">Numéro de téléphone :</label>
      <input type="tel" class="form-control" id="tel" name="tel">
    </div>
    <div class="mb-3">
      <label for="story"></label><textarea id="story" name="story"
      rows="10" cols="113" placeholder="Ecrivez votre message"></textarea>
      <button type="submit" class="btn btn-primary" name="Envoyer">Envoyer</button></div></form>
    

<?php
if(isset($_POST['Envoyer'])) {
    $Prenom = $_POST['Prenom'];
    $Tel = $_POST['tel'];
    echo "Votre demande a bien été prise en compte $Prenom. Nous vous recontacterons dans les plus brefs délais au $Tel. 
    Merci de votre confiance et à bientôt !";
}

?>

</body>


<footer class="footer">   
    <div class="texte">  
    <p>Copyright © 2023</p>     
        <ul>         
        <li><a href="confidentialite.html" class="liens"> Politique de confidentialité </a></li>         
        <li><a href="conditions.html" class="liens"> Conditions d'utilisation </a></li>         
        <li><a href="contact.php" class="liens"> Nous contacter </a></li>  
        <li><a href="#popup" id="subscribe-link"> Inscription Newsletter </a></li>
        <script>
      document.getElementById("subscribe-link").addEventListener("click", function(event) {
      event.preventDefault();
      var popup = window.open("", "Inscription Newsletter", "width=400, height=300");
      popup.document.write("<html><head><style>" +
                         "body { font-family: Arial, sans-serif; }" +
                         "h1 { text-align: center; }" +
                         "form { display: flex; flex-direction: column; align-items: center; }" +
                         "input[type='email'] { padding: 10px; font-size: 18px; margin-bottom: 10px; }" +
                         "input[type='submit'] { padding: 10px 20px; font-size: 18px; background-color: #ffb6c1; border: none; }" +
                         "</style></head><body>" +
                         "<h1>Pour pouvoir vous inscrire, vous devez renseigner votre adresse e-mail :</h1>" +
                         "<form><input type='email' placeholder='Votre adresse e-mail'><input type='submit' value='Souscrire'></form>" +
                         "</body></html>");
       });
        </script>         
        <li><a href="valeurs.php" class="liens"> Nos valeurs </a></li>     
    </ul> 
</div>  
</footer>
</html>